﻿#include "noteitem.h"
#include "itemitem.h"
#include "dbitem.h"
#include "cditem.h"

#include <ntlx/core/note.h>
#include <ntlx/core/database.h>

NoteItem::NoteItem()
  : PlatanusItem()
  , itemTable_()
{
}

NoteItem::NoteItem(const ntlx::ItemTable& itemTable, const QString& text)
  : PlatanusItem(text)
  , itemTable_(itemTable)
{
}

NoteItem::NoteItem(const NoteItem& other)
  : PlatanusItem(other)
  , itemTable_(other.itemTable_)
{
}

NoteItem& NoteItem::operator =(const NoteItem& other)
{
  if (this != &other)
  {
    PlatanusItem::operator =(other);
    itemTable_ = other.itemTable_;
  }
  return *this;
}

NoteItem::~NoteItem()
{
}

NOTEID NoteItem::noteId() const
{
  return itemTable_.match().ID.NoteID;
}

QVariantMap NoteItem::properties() const
{
  QVariantMap map;
  map.insert(
        QObject::tr("Global Instance ID")
        , ntlx::Note::globalInstanceIdText(itemTable_.match().ID).toQString()
        );
  map.insert(
        QObject::tr("Originator ID")
        , ntlx::Note::originatorIdText(itemTable_.match().OriginatorID).toQString()
        );
  map.insert(
        QObject::tr("Note Class")
        , ntlx::Note::noteClassText(itemTable_.match().NoteClass).toQString()
        );
  map.unite(itemTable_.map());
  return map;
}

ContextMenuFlags NoteItem::contextMenuFlags() const
{
  ContextMenuFlags flags;
  flags.setIsExpandable(true);
  return flags;
}

void NoteItem::expand()
{
  DbItem* parentDb = parentItem<DbItem>();
  if (!parentDb)
    return;

  ntlx::Database db(parentDb->filePath());
  ntlx::Note note(db, itemTable_.match().ID.NoteID);
  if (note.open().lastResult().hasError())
    return;

  QList<ntlx::Item> list;
  ntlx::ItemIterator<ntlx::Item, QList<ntlx::Item>> itemIt(&list);
  if (note.scanItems(&itemIt).lastResult().hasError())
    return;

  QMap<ntlx::Text,WORD> flagMap;
  foreach (ntlx::Item item, list)
  {
    if (!flagMap.contains(item.name()))
      flagMap.insert(item.name(), item.flags());
  }

  foreach (ntlx::Text name, flagMap.keys())
  {
    ntlx::Item item = note.getItem(name, flagMap.value(name));
    QString title = QString("%1 (%2) [%3:%4]")
        .arg(name.toQString())
        .arg(item.valueCount())
        .arg(item.type(), 0, 16)
        .arg(item.typeName())
        ;
    ItemItem* child = new ItemItem(item, title);
    appendRow(child);
  }
}
