﻿#include "item/diritem.h"
#include "item/dbitem.h"
#include <ntlx/core/dir.h>
#include <QVariantMap>

DirItem::DirItem()
  : FileItem()
{
}

DirItem::DirItem(const ntlx::FilePath& filePath, const QString& text)
  : FileItem(filePath, text)
{
}

DirItem::DirItem(const ntlx::FilePath& filePath, const ntlx::FileInfo& fileInfo, const QString& text)
  : FileItem(filePath, fileInfo, text)
{
}

DirItem::DirItem(const DirItem& other)
  : FileItem(other)
{
}

DirItem& DirItem::operator =(const DirItem& other)
{
  if (this != &other)
  {
    FileItem::operator =(other);
  }
  return *this;
}

DirItem::~DirItem()
{
}

ContextMenuFlags DirItem::contextMenuFlags() const
{
  ContextMenuFlags flags;
  flags.setIsExpandable(true);
  return flags;
}

void DirItem::expand()
{
  ntlx::Dir dir(filePath_);
  ntlx::FileInfoList list;
  ntlx::SearchIterator<ntlx::FileInfo, ntlx::FileInfoList> dirIt(&list);
  if (dir.listUp(&dirIt).lastResult().hasError())
    return;

  foreach (ntlx::FileInfo fileInfo, list)
  {
    if (fileInfo.type() == DBDIR_TYPE_ITEM_DIRECTORY)
    {
      QString dirPath = fileInfo.path().toQString();
#if defined(Q_OS_WIN)
      QStringList dirPaths = dirPath.split("\\");
#elif defined(Q_OS_UNIX)
      QStringList dirPaths = dirPath.split("/");
#endif
      QString title = "+" + dirPaths.last();
      ntlx::FilePath childFilePath(fileInfo.path(), filePath_.server(), filePath_.port());
      appendRow(new DirItem(childFilePath, fileInfo, title));
    }
  }
  foreach (ntlx::FileInfo fileInfo, list)
  {
    if (fileInfo.type() == DBDIR_TYPE_ITEM_NOTEFILE)
    {
      QString title = fileInfo.title().toQString();
      ntlx::FilePath childFilePath(fileInfo.path(), filePath_.server(), filePath_.port());
      appendRow(new DbItem(childFilePath, fileInfo, title));
    }
  }
}
