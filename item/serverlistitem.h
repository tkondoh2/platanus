﻿#ifndef SERVERLISTITEM_H
#define SERVERLISTITEM_H

#include "platanusitem.h"

class ServerListItem
    : public PlatanusItem
{
public:
  ServerListItem();
  ServerListItem(const QString& text);
  ServerListItem(const ServerListItem& other);
  ServerListItem& operator =(const ServerListItem& other);
  virtual ~ServerListItem();

  virtual ContextMenuFlags contextMenuFlags() const override;
  virtual void expand() override;
  virtual QVariantMap properties() const override;
};

#endif // SERVERLISTITEM_H
