﻿#include "dbitem.h"
#include "item/noteitem.h"

#include <ntlx/core/database.h>
#include <ntlx/core/note.h>
#include <ntlx/core/textlist.h>
#include <ntlx/core/any.h>
#include <ntlx/core/searchiterator.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

DbItem::DbItem()
  : FileItem()
{
}

DbItem::DbItem(const ntlx::FilePath& filePath, const ntlx::FileInfo& fileInfo, const QString& text)
  : FileItem(filePath, fileInfo, text)
{
}

DbItem::DbItem(const DbItem& other)
  : FileItem(other)
{
}

DbItem& DbItem::operator =(const DbItem& other)
{
  if (this != &other)
  {
    FileItem::operator =(other);
  }
  return *this;
}

DbItem::~DbItem()
{
}

ContextMenuFlags DbItem::contextMenuFlags() const
{
  ContextMenuFlags flags;
  flags.setIsExpandable(true);
  return flags;
}

void DbItem::expand()
{
  const QString FieldTitle_Field = QString(FIELD_TITLE);
  const QString NoteClassForm = ntlx::Note::noteClassText(NOTE_CLASS_FORM).toQString();
  const QString NoteClassView = ntlx::Note::noteClassText(NOTE_CLASS_VIEW).toQString();
  const QString DesignFlags_Field = QString(DESIGN_FLAGS);

  ntlx::Database db(filePath_);
  ntlx::ItemTableList list;
  ntlx::SearchIterator<ntlx::ItemTable, ntlx::ItemTableList> noteIt(&list);
  if (db.search(
        &noteIt
        , ntlx::Text()
        , ntlx::Text()
        , SEARCH_SUMMARY
        , NOTE_CLASS_ALL
        ).lastResult().hasError()
      )
    return;

  QMap<QString, QList<ntlx::ItemTableList::const_iterator>> tableListMap;
  for (ntlx::ItemTableList::const_iterator it = list.constBegin()
       ; it != list.constEnd()
       ; ++it
       )
  {
    const ntlx::ItemTable& itemTable = *it;
    QString classTitle = ntlx::Note::noteClassText(itemTable.match().NoteClass).toQString();
    if (classTitle == NoteClassForm || classTitle == NoteClassView)
    {
      ntlx::Any flagsAny = const_cast<ntlx::ItemTable&>(itemTable).map().value(DesignFlags_Field).value<ntlx::Any>();
      QStringList designList = ntlx::Note::designFlagsTextList( flagsAny.getText() ).toQStringList();
      if (!designList.isEmpty())
        classTitle += QString(" %1").arg(designList.join("|"));
    }

    if (!tableListMap.contains(classTitle))
      tableListMap.insert(classTitle, QList<ntlx::ItemTableList::const_iterator>());
    tableListMap[classTitle].append(it);
  }

  foreach (QString classTitle, tableListMap.keys())
  {
    PlatanusItem* subParent = new PlatanusItem(classTitle);
    QList<ntlx::ItemTableList::const_iterator>& list = tableListMap[classTitle];
    foreach (ntlx::ItemTableList::const_iterator it, list)
    {
      ntlx::ItemTable itemTable = *it;
      if (itemTable.map().contains(FieldTitle_Field))
      {
        ntlx::Any any = itemTable.map().value(FieldTitle_Field).value<ntlx::Any>();
        QString fieldTitle = any.getText().toQString();
        NoteItem* child = new NoteItem(itemTable, fieldTitle);
        child->setData(itemTable.map());
        subParent->appendRow(child);
      }
      else
      {
        QString noteidTitle = ntlx::formatId<NOTEID, uint>(itemTable.match().ID.NoteID).toQString();
        NoteItem* child = new NoteItem(itemTable, noteidTitle);
        child->setData(itemTable.map());
        subParent->appendRow(child);
      }
    }
    appendRow(subParent);
  }
}
