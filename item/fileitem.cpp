﻿#include "item/diritem.h"

#include <ntlx/core/note.h>

FileItem::FileItem()
  : PlatanusItem()
  , filePath_()
  , fileInfo_()
{
}

FileItem::FileItem(const ntlx::FilePath& filePath, const QString& text)
  : PlatanusItem(text)
  , filePath_(filePath)
  , fileInfo_()
{
}

FileItem::FileItem(const ntlx::FilePath& filePath, const ntlx::FileInfo& fileInfo, const QString& text)
  : PlatanusItem(text)
  , filePath_(filePath)
  , fileInfo_(fileInfo)
{
}

FileItem::FileItem(const FileItem& other)
  : PlatanusItem(other)
  , filePath_(other.filePath_)
  , fileInfo_(other.fileInfo_)
{
}

FileItem& FileItem::operator =(const FileItem& other)
{
  if (this != &other)
  {
    PlatanusItem::operator =(other);
    filePath_ = other.filePath_;
    fileInfo_ = other.fileInfo_;
  }
  return *this;
}

FileItem::~FileItem()
{
}

QVariantMap FileItem::properties() const
{
  QVariantMap map;
  map.insert(QObject::tr("Port"), filePath_.qPort());
  map.insert(QObject::tr("Server"), filePath_.qServer());
  map.insert(QObject::tr("File path"), filePath_.qPath());
  map.insert(
        QObject::tr("Global Instance ID")
        , ntlx::Note::globalInstanceIdText(fileInfo_.match().ID).toQString()
        );
  map.insert(
        QObject::tr("Originator ID")
        , ntlx::Note::originatorIdText(fileInfo_.match().OriginatorID).toQString()
        );
  map.insert(
        QObject::tr("Note Class")
        , ntlx::Note::noteClassText(fileInfo_.match().NoteClass).toQString()
        );
  map.unite(fileInfo_.map());
  return map;
}
