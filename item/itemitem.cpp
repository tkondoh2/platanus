﻿#include "itemitem.h"
#include "noteitem.h"
#include "dbitem.h"
#include "cditem.h"

#include <ntlx/core/database.h>
#include <ntlx/core/cdrecord.h>
#include <ntlx/core/textlist.h>
#include <ntlx/core/note.h>

ItemItem::ItemItem()
  : PlatanusItem()
  , item_()
{
}

ItemItem::ItemItem(const ntlx::Item& item, const QString& title)
  : PlatanusItem(title)
  , item_(item)
{
}

ItemItem::ItemItem(const ItemItem& other)
  : PlatanusItem(other)
  , item_(other.item_)
{
}

ItemItem& ItemItem::operator =(const ItemItem& other)
{
  if (this != &other)
  {
    PlatanusItem::operator =(other);
    item_ = other.item_;
  }
  return *this;
}

ItemItem::~ItemItem()
{
}

ContextMenuFlags ItemItem::contextMenuFlags() const
{
  ContextMenuFlags flags;
  if (item_.type() == TYPE_COMPOSITE)
    flags.setIsExpandable(true);
  return flags;
}

void ItemItem::expand()
{
  NoteItem* parentNote = parentItem<NoteItem>();
  if (!parentNote)
    return;

  DbItem* parentDb = parentNote->parentItem<DbItem>();
  if (!parentDb)
    return;

  ntlx::Database db(parentDb->filePath());
  if (db.open().lastResult().hasError())
    return;

  ntlx::Note note(db, parentNote->noteId());

  // @TODO リッチテキストのCDデータを巡回取得するコードを書く。
  QList<ntlx::CdRecord> list;
  if (item_.getAllCdRecords(note, list).lastResult().hasError())
    return;

  foreach (ntlx::CdRecord cdrec, list)
  {
    QString title = cdrec.signatureName().toQString();
    CdItem* child = new CdItem(cdrec, title);
    appendRow(child);
  }
}

QVariantMap ItemItem::properties() const
{
  QVariantMap map;
  map.insert(QObject::tr("Name"), QVariant::fromValue<ntlx::Text>(item_.name()));
  map.insert(QObject::tr("Flags"), QVariant::fromValue<ntlx::TextList>(item_.flagNames()));
  map.insert(QObject::tr("Type"), item_.typeName());

  for (int i = 0; i < item_.valueCount(); ++i)
  {
    map.insert(
          QObject::tr("Value(%1)").arg(i)
          , QVariant::fromValue<ntlx::Any>(item_.valueAt(i))
          );
  }

  return map;
}
