﻿#ifndef ITEMITEM_H
#define ITEMITEM_H

#include "platanusitem.h"
#include <ntlx/core/file.h>
#include <ntlx/core/item.h>

class ItemItem
    : public PlatanusItem
{
public:
  ItemItem();
  ItemItem(const ntlx::Item& item, const QString& title);
  ItemItem(const ItemItem& other);
  ItemItem& operator =(const ItemItem& other);
  virtual ~ItemItem();

  virtual ContextMenuFlags contextMenuFlags() const override;
  virtual void expand() override;

  virtual QVariantMap properties() const override;

protected:
  ntlx::Item item_;
};

#endif // ITEMITEM_H
