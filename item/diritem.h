﻿#ifndef DIRITEM_H
#define DIRITEM_H

#include "item/fileitem.h"

class DirItem
    : public FileItem
{
public:
  DirItem();
  DirItem(const ntlx::FilePath& filePath, const QString& text);
  DirItem(const ntlx::FilePath& filePath, const ntlx::FileInfo& fileInfo, const QString& text);
  DirItem(const DirItem& other);
  DirItem& operator =(const DirItem& other);
  virtual ~DirItem();

  virtual ContextMenuFlags contextMenuFlags() const override;
  virtual void expand() override;
};

#endif // DIRITEM_H
