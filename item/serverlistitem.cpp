﻿#include "serverlistitem.h"
#include "item/diritem.h"
#include <ntlx/core/serveriterator.h>
#include <ntlx/core/distinguishedname.h>
#include <ntlx/core/file.h>

ServerListItem::ServerListItem()
  : PlatanusItem()
{
}

ServerListItem::ServerListItem(const QString& text)
  : PlatanusItem(text)
{
}

ServerListItem::ServerListItem(const ServerListItem& other)
  : PlatanusItem(other)
{
}

ServerListItem& ServerListItem::operator =(const ServerListItem& other)
{
  if (this != &other)
  {
    PlatanusItem::operator =(other);
  }
  return *this;
}

ServerListItem::~ServerListItem()
{
}

ContextMenuFlags ServerListItem::contextMenuFlags() const
{
  ContextMenuFlags flags;
  flags.setIsExpandable(true);
  return flags;
}

void ServerListItem::expand()
{
  QList<ntlx::DName> list;
  ntlx::ServerIterator<ntlx::DName, QList<ntlx::DName>> svIt(&list);
  if (svIt().lastResult().hasError())
    return;

  foreach (ntlx::DName server, list)
  {
    QString serverName = server.abbreviated().toQString();
    ntlx::FilePath filePath(QString(), serverName);
    appendRow(new DirItem(filePath, serverName));
  }
}

QVariantMap ServerListItem::properties() const
{
  return QVariantMap();
}
