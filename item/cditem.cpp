﻿#include "cditem.h"

#include <ntlx/core/cd/cdtext.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ods.h>
#include <editods.h>

#if defined(NT)
#pragma pack(pop)
#endif

#include <QtDebug>

CdItem::CdItem()
  : PlatanusItem()
  , cd_()
{
}

CdItem::CdItem(const ntlx::CdRecord& cd, const QString& title)
  : PlatanusItem(title)
  , cd_(cd)
{
}

CdItem::CdItem(const CdItem& other)
  : PlatanusItem(other)
  , cd_(other.cd_)
{
}

CdItem& CdItem::operator =(const CdItem& other)
{
  if (this != &other)
  {
    PlatanusItem::operator =(other);
    cd_ = other.cd_;
  }
  return *this;
}

CdItem::~CdItem()
{
}

QVariantMap CdItem::properties() const
{
  QVariantMap map;
  switch (cd_.signature())
  {
  case SIG_CD_PABDEFINITION: insertCDPABDEFINITION(cd_, map); break;
  case SIG_CD_PARAGRAPH: insertCDPARAGRAPH(cd_, map); break;
  case SIG_CD_TEXT: insertCDTEXT(cd_, map); break;
  }

  return map;
}

template <typename T>
void insertMap(QVariantMap& map, T value, const QString& name)
{
  map.insert(name, QVariant(value));
}

void CdItem::insertCDPABDEFINITION(const ntlx::CdRecord& cd, QVariantMap &map)
{
  CDPABDEFINITION data = cd.extract<CDPABDEFINITION, _CDPABDEFINITION>();
  insertMap<uint>(map, data.PABID, QObject::tr("PABID"));
  insertMap<uint>(map, data.JustifyMode, QObject::tr("JustifyMode"));
  insertMap<uint>(map, data.LineSpacing, QObject::tr("LineSpacing"));
  insertMap<uint>(map, data.ParagraphSpacingBefore, QObject::tr("ParagraphSpacingBefore"));
  insertMap<uint>(map, data.ParagraphSpacingAfter, QObject::tr("ParagraphSpacingAfter"));
  insertMap<uint>(map, data.LeftMargin, QObject::tr("LeftMargin"));
  insertMap<uint>(map, data.RightMargin, QObject::tr("RightMargin"));
  insertMap<uint>(map, data.FirstLineLeftMargin, QObject::tr("FirstLineLeftMargin"));
  insertMap<uint>(map, data.Tabs, QObject::tr("Tabs"));
  for (int i = 0; i < MAXTABS; ++i)
    insertMap<uint>(map, data.Tab[i], QObject::tr("Tab[%1]").arg(i, 2, 10, QChar('0')));
  insertMap<uint>(map, data.Flags, QObject::tr("Flags"));
  insertMap<uint>(map, data.TabTypes, QObject::tr("TabTypes"));
  insertMap<uint>(map, data.Flags2, QObject::tr("Flags2"));
}

void CdItem::insertCDPABREFERENCE(const ntlx::CdRecord& cd, QVariantMap& map)
{
  CDPABREFERENCE ref = cd.extract<CDPABREFERENCE, _CDPABREFERENCE>();
  insertMap<uint>(map, ref.PABID, QObject::tr("PABID"));
}

void CdItem::insertCDPARAGRAPH(const ntlx::CdRecord& /*cd*/, QVariantMap& /*map*/)
{
}

void CdItem::insertCDTEXT(const ntlx::CdRecord& cd, QVariantMap &map)
{
  ntlx::CdText text;
  text.read(cd);
  insertMap<QString>(map, text.value().toQString(), QObject::tr("Text"));
  insertMap<uint>(map, text.fontFaceID(), QObject::tr("Font Face ID"));
  insertMap<uint>(map, text.fontSize(), QObject::tr("Font Size"));
  insertMap<uint>(map, text.fontColor(), QObject::tr("Font Color"));
  insertMap<bool>(map, text.fontIsPlain(), QObject::tr("Font is Plain"));
  insertMap<bool>(map, text.fontIsUnderline(), QObject::tr("Font is Underline"));
  insertMap<bool>(map, text.fontIsItalic(), QObject::tr("Font is Italic"));
  insertMap<bool>(map, text.fontIsBold(), QObject::tr("Font is Bold"));
  insertMap<bool>(map, text.fontIsStrikeOut(), QObject::tr("Font is StrikeOut"));
  insertMap<bool>(map, text.fontIsSuperScript(), QObject::tr("Font is SuperScript"));
  insertMap<bool>(map, text.fontIsSubScript(), QObject::tr("Font is SubScript"));
  insertMap<bool>(map, text.fontIsEffect(), QObject::tr("Font is Effect"));
  insertMap<bool>(map, text.fontIsEmboss(), QObject::tr("Font is Emboss"));
  insertMap<bool>(map, text.fontIsExtrude(), QObject::tr("Font is Extrude"));
  insertMap<uint>(map, text.fontShadowOffset(), QObject::tr("Font Shadow Offset"));
}
