﻿#ifndef NOTEITEM_H
#define NOTEITEM_H

#include "platanusitem.h"
#include <ntlx/core/file.h>
#include <ntlx/core/itemtable.h>

class NoteItem
    : public PlatanusItem
{
public:
  NoteItem();
  NoteItem(const ntlx::ItemTable& itemTable, const QString& text);
  NoteItem(const NoteItem& other);
  NoteItem& operator =(const NoteItem& other);
  virtual ~NoteItem();

  virtual QVariantMap properties() const override;

  virtual ContextMenuFlags contextMenuFlags() const override;
  virtual void expand() override;

  NOTEID noteId() const;

protected:
  ntlx::ItemTable itemTable_;
};

#endif // NOTEITEM_H
