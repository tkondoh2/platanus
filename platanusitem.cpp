﻿#include "platanusitem.h"

ContextMenuFlags::ContextMenuFlags()
  : isExpandable_(false)
{
}

bool ContextMenuFlags::isExpandable() const
{
  return isExpandable_;
}

void ContextMenuFlags::setIsExpandable(bool isExpandable)
{
  isExpandable_ = isExpandable;
}

ContextMenuFlags PlatanusItem::contextMenuFlags() const
{
  return ContextMenuFlags();
}

void PlatanusItem::expand()
{
}

QVariantMap PlatanusItem::properties() const
{
  QVariant v = data();
  if (v.canConvert<QVariantMap>())
    return v.toMap();
  return QVariantMap();
}
