﻿#include "platanusview.h"

PlatanusView::PlatanusView(QWidget* parent)
  : QTreeView(parent)
{
  // コンテキストメニューポリシーをカスタムに設定
  setContextMenuPolicy(Qt::CustomContextMenu);
}
