﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "platanustreemodel.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief メインウィンドウクラス
 */
class MainWindow
    : public QMainWindow
{
  Q_OBJECT

public:
  /**
   * @brief コンストラクタ
   * @param parent 親ウィジェットへのポインタ
   */
  explicit MainWindow(QWidget* parent = 0);

  /**
   * @brief デストラクタ
   */
  ~MainWindow();

  /**
   * @brief 初期化処理
   */
  void initialize();

public slots:

  void showProperties(const QModelIndex& index);
  void writeProperties(const QVariant& data);

  void addLocalItem();
  void addServersItem();

  void expand();

  /**
   * @brief コンテキストメニューの表示
   * @param pos マウスをクリックした場所
   */
  void contextMenu(const QPoint& pos);

private:
  Ui::MainWindow* ui_;
  PlatanusTreeModel model_;
  QStandardItem* local_;
  QStandardItem* servers_;
  QStandardItemModel properties_;
};

#endif // MAINWINDOW_H
