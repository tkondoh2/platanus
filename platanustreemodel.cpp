﻿#include "platanustreemodel.h"
#include "platanusitem.h"

PlatanusTreeModel::PlatanusTreeModel(QObject* parent)
  : QStandardItemModel(parent)
{
  // ヘッダーラベルを設定する
  setHorizontalHeaderLabels(QStringList() << tr("Items"));
}

void PlatanusTreeModel::showProperties(const QModelIndex& index)
{
  if (PlatanusItem* pItem = reinterpret_cast<PlatanusItem*>(
        itemFromIndex(index)
        ))
  {
    emit writeProperties(pItem->properties());
  }
}
