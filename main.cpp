﻿#include "mainwindow.h"
#include <QApplication>
#include <ntlx/core/main.h>

#include <QtDebug>

/**
 * @brief エントリーポイント
 * @param argc コマンドライン引数の数
 * @param argv コマンドライン引数の格納先
 * @return エラーコード
 */
int main(int argc, char* argv[])
{
  ntlx::Main notesMain(argc, argv);

  // QApplicationの生成
  QApplication app(argc, argv);

  // アプリケーション情報の登録
  app.setOrganizationName("Chiburu Systems");
  app.setApplicationName("Platanus");

  // メインウィンドウの生成、表示
  MainWindow mainWin;
  mainWin.show();

  // イベントループの実行
  return app.exec();
}
