#-------------------------------------------------
#
# Project created by QtCreator 2018-02-20T21:56:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = platanus
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    platanusview.cpp \
    platanustreemodel.cpp \
    item/fileitem.cpp \
    item/diritem.cpp \
    item/serverlistitem.cpp \
    item/dbitem.cpp \
    platanusitem.cpp \
    item/noteitem.cpp \
    item/itemitem.cpp \
    item/cditem.cpp

HEADERS  += mainwindow.h \
    platanusview.h \
    platanustreemodel.h \
    platanusitem.h \
    item/fileitem.h \
    item/diritem.h \
    item/serverlistitem.h \
    item/dbitem.h \
    item/noteitem.h \
    item/itemitem.h \
    item/cditem.h

FORMS    += mainwindow.ui

DISTFILES += \
    .gitignore

#
# Notes API用マクロ定義
#
win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -lnotes -lntlx_core
