﻿#ifndef PLATANUSVIEW_H
#define PLATANUSVIEW_H

#include <QTreeView>

/**
 * @brief Platanusビュークラス
 */
class PlatanusView
    : public QTreeView
{
  Q_OBJECT
public:
  /**
   * @brief デフォルトコンストラクタ
   * @param parent 親ウィジェットへのポインタ
   */
  explicit PlatanusView(QWidget* parent = 0);

signals:

public slots:
};

#endif // PLATANUSVIEW_H
